package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import model.Elem;

public class SearchTextField implements EventHandler<KeyEvent> {

	private final TextField textField;
	private final ListView<Elem> listView;
	private final ObservableList<Elem> items;

	public SearchTextField(ListView<Elem> listView, TextField textField) {
		this.listView = listView;
		this.textField = textField;
		this.items = listView.getItems();

		this.textField.setOnKeyReleased(this);
	}

	@Override
	public void handle(KeyEvent event) {
		String currentText = textField.getText();
		System.out.println(currentText);

		ObservableList<Elem> filteredElemek = FXCollections.observableArrayList();

		for (Elem e : this.items) {
			if (e.toString().toLowerCase().contains(currentText.toLowerCase())) {
				filteredElemek.add(e);
			}
		}

		this.listView.setItems(filteredElemek);

	}

}