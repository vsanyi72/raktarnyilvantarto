package Controller;

import java.io.IOException;

import application.SearchTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Elem;
import javafx.scene.control.ListView;

public class RaktarController {
	@FXML
	private Button btnUjBejegyzes;
	@FXML
	private Button btnModositas;
	@FXML
	private Button btnTorles;
	@FXML
	public ListView<Elem> lvRaktar;
	@FXML
	private TextField tfKereses;

	@FXML
	public void initialize() {
		ObservableList<Elem> dummyElemek = FXCollections.observableArrayList(new Elem("Zokni", "12", "Piros"),
				new Elem("Sál", "2", "Lila"), new Elem("Nadrág", "1", "Zöld"));

		this.lvRaktar.setItems(dummyElemek);

		new SearchTextField(lvRaktar, tfKereses);

	}

	@FXML
	public void pressedUjBejegyzes() {
		adatCsere.getInstance().setModositas(false);

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/View/Bevitel.fxml"));
			AnchorPane aPane = (AnchorPane) fxmlLoader.load();
			Scene scene = new Scene(aPane);
			Stage stage = new Stage();
			stage.initOwner(this.btnUjBejegyzes.getScene().getWindow());
			stage.initStyle(StageStyle.DECORATED);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setTitle("Felvétel");
			stage.setScene(scene);
			stage.show();
			stage.setOnCloseRequest((event) -> {
				ObservableList<Elem> elemek = FXCollections.observableArrayList();
				Elem elem = new Elem();
				elem.setMegnevezes(adatCsere.getInstance().getMegnevezes());
				elem.setDb(adatCsere.getInstance().getDb());
				elem.setMegjegyzes(adatCsere.getInstance().getMegjegyzes());
				elemek = lvRaktar.getItems();
				elemek.add(elem);
				lvRaktar.setItems(elemek);
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void pressedTorol() {
		int selectedIndex = lvRaktar.getSelectionModel().getSelectedIndex();
		if (selectedIndex > -1) {
			lvRaktar.getItems().remove(selectedIndex);
		} else {
			int utolsoIndex = lvRaktar.getItems().size() - 1;
			if (utolsoIndex >= 0) {
				lvRaktar.getItems().remove(utolsoIndex);
			}
		}

	}

	@FXML
	public void pressedModositas() {
		adatCsere.getInstance().setModositas(true);
		int selectedIndex = lvRaktar.getSelectionModel().getSelectedIndex();
		if (selectedIndex < 0) {
			selectedIndex = lvRaktar.getItems().size() - 1;
		}
		Elem elem = lvRaktar.getItems().get(selectedIndex);
		adatCsere.getInstance().setSelectedItem(selectedIndex);
		adatCsere.getInstance().setMegnevezes(elem.getMegnevezes());
		adatCsere.getInstance().setDb(elem.getDb());
		adatCsere.getInstance().setMegjegyzes(elem.getMegjegyzes());

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/View/Bevitel.fxml"));
			AnchorPane aPane = (AnchorPane) fxmlLoader.load();
			Scene scene = new Scene(aPane);
			Stage stage = new Stage();
			stage.initOwner(this.btnModositas.getScene().getWindow());
			stage.initStyle(StageStyle.DECORATED);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setTitle("Módosítás");
			stage.setScene(scene);
			stage.show();
			stage.setOnCloseRequest((event) -> {
				ObservableList<Elem> elemek = FXCollections.observableArrayList();
				elem.setMegnevezes(adatCsere.getInstance().getMegnevezes());
				elem.setDb(adatCsere.getInstance().getDb());
				elem.setMegjegyzes(adatCsere.getInstance().getMegjegyzes());
				elemek = lvRaktar.getItems();
				elemek.remove(adatCsere.getInstance().getSelectedItem());
				elemek.add(adatCsere.getInstance().getSelectedItem(), elem);
				lvRaktar.setItems(elemek);

			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
