package Controller;

public final class adatCsere {

	private final static adatCsere INSTANCE = new adatCsere();

	private adatCsere() {
	};

	public static adatCsere getInstance() {
		return INSTANCE;
	}

	private String megnevezes;
	private String db;
	private String megjegyzes;
	private int selectedItem;
	private boolean modositas;

	public String getMegnevezes() {
		return megnevezes;
	}

	public void setMegnevezes(String megnevezes) {
		this.megnevezes = megnevezes;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getMegjegyzes() {
		return megjegyzes;
	}

	public void setMegjegyzes(String megjegyzes) {
		this.megjegyzes = megjegyzes;
	}

	public int getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(int selectedItem) {
		this.selectedItem = selectedItem;
	}

	public boolean isModositas() {
		return modositas;
	}

	public void setModositas(boolean modositas) {
		this.modositas = modositas;
	}

}
