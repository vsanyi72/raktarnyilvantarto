package Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class BevitelController {

	@FXML
	private Button btnMent;
	@FXML
	private Button btnMegse;
	@FXML
	private TextField tfMegnevezes;
	@FXML
	private TextField tfDb;
	@FXML
	private TextField tfMegjegyzes;

	@FXML
	public void megse() {
		this.btnMegse.getScene().getWindow().hide();
	}

	@FXML
	private void initialize() {
		if (adatCsere.getInstance().isModositas() == true) {
			tfDb.setText(adatCsere.getInstance().getDb());
			tfMegnevezes.setText(adatCsere.getInstance().getMegnevezes());
			tfMegjegyzes.setText(adatCsere.getInstance().getMegjegyzes());
		}

	}

	@FXML
	public void ment() {
		adatCsere.getInstance().setDb(tfDb.getText());
		adatCsere.getInstance().setMegnevezes(tfMegnevezes.getText());
		adatCsere.getInstance().setMegjegyzes(tfMegjegyzes.getText());

	}

}
