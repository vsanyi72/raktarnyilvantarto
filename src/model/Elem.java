package model;

public class Elem {

	private String megnevezes;
	private String db;
	private String megjegyzes;

	public Elem(String megnevezes, String db, String megjegyzes) {
		super();
		this.megnevezes = megnevezes;
		this.db = db;
		this.megjegyzes = megjegyzes;
	}

	public Elem() {
		super();
	}

	public String getMegnevezes() {
		return megnevezes;
	}

	public void setMegnevezes(String megnevezes) {
		this.megnevezes = megnevezes;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getMegjegyzes() {
		return megjegyzes;
	}

	public void setMegjegyzes(String megjegyzes) {
		this.megjegyzes = megjegyzes;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(megnevezes).append("(" + db + ")").append(megjegyzes);
		return sb.toString();
	}

}
